const LOCAL_GLOBAL = '@sbpAdmin';

const storage = {
  token: `${LOCAL_GLOBAL}:token`,
};

const resetStorages = () => {
  localStorage.removeItem(storage.token);
};

export { LOCAL_GLOBAL, storage, resetStorages };
