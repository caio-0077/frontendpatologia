import { FieldErrors } from 'react-hook-form';

type ErrorsType = FieldErrors | undefined;

export type ErrorType = {
  response?: {
    data?: { message?: string; statusCode?: number; type?: string };
  };
};

interface ServerError {
  type: 'error';
  message: string;
}

type FormatSize = string | undefined;

const getErrorMessageYup = (errors: ErrorsType, name: string): string => {
  if (!errors?.[name]) return '';
  const message = String(errors[name]?.message) || '';
  return message;
};

const getErrorMessageServer = (error: ErrorType): ServerError => {
  if (!error?.response?.data?.message) {
    return {
      type: 'error',
      message:
        'Ocorreu um erro ao processar a sua solicitação, tente novamente.',
    };
  }
  const errorMessage = error?.response?.data?.message;
  return { type: 'error', message: errorMessage };
};

const formatCNPJ = (value: string | undefined): string => {
  if (!value) return '';
  const cleanedCNPJ = value.replace(/\D/g, '');
  const formattedCNPJ =
    cleanedCNPJ.length >= 12
      ? `${cleanedCNPJ.slice(0, 2)}.${cleanedCNPJ.slice(
          2,
          5,
        )}.${cleanedCNPJ.slice(5, 8)}/${cleanedCNPJ.slice(
          8,
          12,
        )}-${cleanedCNPJ.slice(12)}`
      : cleanedCNPJ;
  return formattedCNPJ;
};

const removePointsCNPJ = (value: string | undefined): string => {
  if (!value) return '-';
  return value.replace(/\D/g, '');
};

const formatSizeUnits = (bytes: number | undefined): FormatSize => {
  if (!bytes) return undefined;
  if (bytes >= 1073741824) {
    return (bytes / 1073741824).toFixed(2) + ' GB';
  } else if (bytes >= 1048576) {
    return (bytes / 1048576).toFixed(2) + ' MB';
  } else if (bytes >= 1024) {
    return (bytes / 1024).toFixed(2) + ' KB';
  } else if (bytes > 1) {
    return bytes + ' bytes';
  } else if (bytes === 1) {
    return bytes + ' byte';
  } else {
    return '0 bytes';
  }
};

export {
  formatCNPJ,
  formatSizeUnits,
  removePointsCNPJ,
  getErrorMessageYup,
  getErrorMessageServer,
};
