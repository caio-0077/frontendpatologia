import { useState, useEffect } from 'react';
import { Layout } from 'components';
import { useParams } from 'react-router-dom';
import { CardImage, CardForm } from './components';

type StackType = 'login' | 'forgot-password' | 'reset-password';
type TabType = 'forgot-password' | 'reset-password';

export const SignIn = () => {
  const { tab } = useParams<{ tab?: TabType }>();
  const [currentStack, setCurrentStack] = useState<StackType>('login');

  useEffect(() => {
    if (tab) {
      setCurrentStack(tab);
    } else {
      setCurrentStack('login');
    }
  }, [tab]);

  return (
    <Layout.Container background="bg-background-0">
      <Layout.Content background="bg-background-0" noShadow>
        <div className="flex items-center justify-center flex-1">
          <CardImage />
          <CardForm currentStack={currentStack} />
        </div>
      </Layout.Content>
    </Layout.Container>
  );
};
