export const CardImage = () => {
  return (
    <div className="flex items-start justify-between flex-col p-10 bg-[url('assets/login.png')] max-h-[500px] h-full max-w-[464px] w-full rounded-l-2xl shadow-[0_4px_4px_0px_rgba(0,0,0,0.05)]">
      <span className="text-5xl font-medium text-white-0">
        Plataforma
        <br />
        de índices
      </span>
      <span className="text-xs text-white-0">
        Produzido por Sociedade Brasileira de Patologia. 2023.
      </span>
    </div>
  );
};
