import { FC } from 'react';

import { FormElements } from 'components/form-elements';
import { Controller, Control, FieldErrors } from 'react-hook-form';

type FormValues = {
  password: string;
  confirmPassword: string;
};

interface RegisterNewPasswordFromProps {
  control: Control<FormValues>;
  errors: FieldErrors;
}

export const RegisterNewPasswordFrom: FC<RegisterNewPasswordFromProps> = ({
  control,
  errors,
}) => {
  return (
    <div className="flex flex-col w-full gap-4">
      <Controller
        control={control}
        name="password"
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            label="Senha*"
            password
            name={name}
            value={value}
            errors={errors}
            onChange={onChange}
          />
        )}
      />
      <Controller
        control={control}
        name="confirmPassword"
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            label="Confirmar senha*"
            password
            name={name}
            value={value}
            errors={errors}
            onChange={onChange}
          />
        )}
      />
    </div>
  );
};
