import { LoginFrom } from './login-form';
import { RecoverPasswordForm } from './recover-password-form';
import { RegisterNewPasswordFrom } from './register-new-password-form';

export const Form = {
  Login: LoginFrom,
  RecoverPassword: RecoverPasswordForm,
  RegisterNewPassword: RegisterNewPasswordFrom,
};
