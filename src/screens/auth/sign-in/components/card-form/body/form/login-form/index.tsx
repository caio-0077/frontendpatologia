import { FC } from 'react';
import { FormElements } from 'components/form-elements';
import { Controller, Control, FieldErrors } from 'react-hook-form';

type FormValues = {
  email: string;
  password: string;
};

interface LoginFromProps {
  control: Control<FormValues>;
  errors: FieldErrors;
}

export const LoginFrom: FC<LoginFromProps> = ({ control, errors }) => {
  return (
    <div className="flex flex-col w-full gap-4">
      <Controller
        control={control}
        name="email"
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            label="Email"
            name={name}
            value={value}
            errors={errors}
            onChange={onChange}
          />
        )}
      />
      <Controller
        control={control}
        name="password"
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            password
            label="Senha"
            name={name}
            value={value}
            errors={errors}
            onChange={onChange}
          />
        )}
      />
    </div>
  );
};
