import { FC } from 'react';

import { FormElements } from 'components/form-elements';
import { Controller, Control, FieldErrors } from 'react-hook-form';

type FormValues = {
  email: string;
};

interface RecoverPasswordFormProps {
  control: Control<FormValues>;
  errors?: FieldErrors;
}

export const RecoverPasswordForm: FC<RecoverPasswordFormProps> = ({
  control,
  errors,
}) => {
  return (
    <div className="flex w-full">
      <Controller
        control={control}
        name="email"
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            label="Email*"
            name={name}
            value={value}
            errors={errors}
            onChange={onChange}
          />
        )}
      />
    </div>
  );
};
