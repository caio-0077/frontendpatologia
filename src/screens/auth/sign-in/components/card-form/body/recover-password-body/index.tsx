import { FC, useState } from 'react';

import { Form } from '../form';
import { Icon } from 'components';
import { Button } from 'components/button';
import { useAuth } from 'hooks/contexts/use-auth';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { recoverPasswordValidation } from '../validation';

type FormValues = {
  email: string;
};

interface RecoverPasswordBodyProps {
  goBackLogin: () => void;
}

export const RecoverPasswordBody: FC<RecoverPasswordBodyProps> = ({
  goBackLogin,
}) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({
    resolver: yupResolver(recoverPasswordValidation),
  });
  const { forgotPassword, isForgotPasswordLoading } = useAuth();
  const [hasEmailSent, setHasEmailSent] = useState(false);

  const onSubmit: SubmitHandler<FormValues> = async (data) => {
    try {
      await forgotPassword(data);
      setHasEmailSent(true);
    } catch (error) {
      throw new Error();
    }
  };

  return (
    <>
      {!hasEmailSent && (
        <form
          className="flex flex-col items-center justify-center gap-14 w-full h-full"
          onSubmit={handleSubmit(onSubmit)}
        >
          <span className="text-2xl text-neutral-10">Recupere sua senha</span>
          <div className="flex gap-10 flex-col w-full items-center justify-center">
            <Form.RecoverPassword control={control} errors={errors} />
            <Button
              width="w-[200px]"
              type="submit"
              isLoading={isForgotPasswordLoading}
            >
              Enviar
            </Button>
          </div>
          <button
            className="hover:opacity-80"
            onClick={goBackLogin}
            type="button"
          >
            <span className="text-sm text-primary-40 font-bold">
              Voltar para o login
            </span>
          </button>
        </form>
      )}
      {hasEmailSent && (
        <div className="flex flex-col items-center justify-center gap-6 w-full h-full">
          <Icon name="verified" size="large" className="text-tertiary-80" />
          <span className="text-2xl text-neutral-10">Recupere sua senha</span>
          <span className="text-base text-neutral_variant-30">
            Enviamos um email contendo o link para você recuperar sua senha.
            Caso não encontre na sua caixa de entrada, confira também o spam.
          </span>
        </div>
      )}
    </>
  );
};
