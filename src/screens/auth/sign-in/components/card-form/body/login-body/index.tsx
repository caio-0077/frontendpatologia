import { FC } from 'react';

import { Form } from '../form';
import { useAuth } from 'hooks/contexts/use-auth';
import { Button } from 'components/button';
import { useNavigate } from 'react-router-dom';
import { loginValidation } from '../validation';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { enumRoutesLaboratories } from 'routes/enum-routes';

type FormValues = {
  email: string;
  password: string;
};

interface LoginBodyProps {
  goToForgotPassword: () => void;
}

export const LoginBody: FC<LoginBodyProps> = ({ goToForgotPassword }) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({
    resolver: yupResolver(loginValidation),
  });
  const navigate = useNavigate();
  const { signIn, isAuthLoading } = useAuth();

  const onSubmit: SubmitHandler<FormValues> = async (data) => {
    try {
      await signIn(data);
      navigate(enumRoutesLaboratories.LABORATORIES);
    } catch {
      throw new Error();
    }
  };

  return (
    <form
      className="flex flex-col items-center justify-center gap-14 w-full h-full"
      onSubmit={handleSubmit(onSubmit)}
    >
      <span className="text-2xl text-neutral-10">Acesse sua conta</span>
      <div className="flex flex-col items-center justify-center gap-10 w-full">
        <Form.Login control={control} errors={errors} />
        <div className="flex flex-col items-center justify-center gap-6">
          <span className="text-sm text-neutral-10">
            Esqueceu sua senha?{' '}
            <button
              type="button"
              className="font-bold text-primary-40 hover:opacity-80"
              onClick={goToForgotPassword}
            >
              Clique aqui
            </button>
          </span>
          <Button width="w-[200px]" type="submit" isLoading={isAuthLoading}>
            Entrar
          </Button>
        </div>
      </div>
    </form>
  );
};
