import { LoginBody } from './login-body';
import { RecoverPasswordBody } from './recover-password-body';
import { RegisterNewPasswordBody } from './register-new-password-body';

export const Body = {
  Login: LoginBody,
  RecoverPassword: RecoverPasswordBody,
  RegisterNewPassword: RegisterNewPasswordBody,
};
