import { useState } from 'react';

import { Form } from '../form';
import { Icon } from 'components';
import { Button } from 'components/button';
import { useAuth } from 'hooks/contexts/use-auth';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useParams, useNavigate } from 'react-router-dom';
import { registerNewPasswordValidation } from '../validation';

type FormValues = {
  password: string;
  confirmPassword: string;
};

export const RegisterNewPasswordBody = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({
    resolver: yupResolver(registerNewPasswordValidation),
  });
  const navigate = useNavigate();
  const { token } = useParams<{ token?: string }>();
  const { resetPassword, isResetPasswordLoading } = useAuth();
  const [hasPasswordChanged, setHasPasswordChanged] = useState(false);

  const onSubmit: SubmitHandler<FormValues> = async (data) => {
    try {
      await resetPassword({ ...data, token });
      setHasPasswordChanged(true);
    } catch (error) {
      throw new Error();
    }
  };

  const goToLogin = () => {
    navigate('/login');
  };

  return (
    <>
      {!hasPasswordChanged && (
        <form
          className="flex flex-col items-center justify-center gap-14 w-full h-full"
          onSubmit={handleSubmit(onSubmit)}
        >
          <span className="text-2xl text-neutral-10">
            Cadastre sua nova senha
          </span>
          <div className="flex gap-10 flex-col w-full items-center justify-center">
            <Form.RegisterNewPassword control={control} errors={errors} />
            <Button
              width="w-[200px]"
              type="submit"
              isLoading={isResetPasswordLoading}
            >
              Salvar
            </Button>
          </div>
        </form>
      )}
      {hasPasswordChanged && (
        <div className="flex flex-col items-center justify-center gap-10 w-full h-full">
          <div className="flex flex-col items-center justify-center gap-6 w-full">
            <Icon name="verified" size="large" className="text-tertiary-80" />
            <span className="text-2xl text-neutral-10">Senha salva!</span>
            <span className="text-base text-neutral_variant-30">
              Acesse sua conta com seu email e nova senha para entrar na
              plataforma.
            </span>
          </div>
          <Button
            onClick={goToLogin}
            variant="outlined"
            width="w-[200px]"
            type="button"
          >
            Acessar conta
          </Button>
        </div>
      )}
    </>
  );
};
