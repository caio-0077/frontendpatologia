import * as yup from 'yup';

const loginValidation = yup.object().shape({
  email: yup.string().required('Email é obrigatório').email('Email inválido'),
  password: yup
    .string()
    .required('Senha é obrigatória')
    .min(6, 'A senha incorreta'),
});

const recoverPasswordValidation = yup.object().shape({
  email: yup.string().required('Email é obrigatório').email('Email inválido'),
});

const registerNewPasswordValidation = yup.object().shape({
  password: yup
    .string()
    .required('Senha é obrigatória')
    .min(6, 'No mínimo 6 dígitos'),
  confirmPassword: yup
    .string()
    .test(
      'confirmPassword',
      'Senhas não coincidem',
      (value, { parent: { password } }) => value === password,
    )
    .required('Senha é obrigatória')
    .min(6, 'No mínimo 6 dígitos'),
});

export {
  loginValidation,
  recoverPasswordValidation,
  registerNewPasswordValidation,
};
