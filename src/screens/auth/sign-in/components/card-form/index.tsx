import { FC } from 'react';

import { Body } from './body';
import { useNavigate } from 'react-router-dom';
import { enumRoutesAuth } from 'routes/enum-routes';

interface CardFormProps {
  currentStack?: string;
}

export const CardForm: FC<CardFormProps> = ({ currentStack }) => {
  const navigate = useNavigate();

  const goBackLogin = () => {
    navigate(enumRoutesAuth.LOGIN);
  };

  const goToForgotPassword = () => {
    navigate(`${enumRoutesAuth.FORGOT.replace(':tab', 'forgot-password')}`);
  };

  return (
    <div className="px-14 p-10 bg-white rounded-e-2xl h-[500px] w-[464px] shadow-[0_4px_4px_0px_rgba(0,0,0,0.05)]">
      {currentStack === 'login' && (
        <Body.Login goToForgotPassword={goToForgotPassword} />
      )}
      {currentStack === 'forgot-password' && (
        <Body.RecoverPassword goBackLogin={goBackLogin} />
      )}
      {currentStack === 'reset-password' && <Body.RegisterNewPassword />}
    </div>
  );
};
