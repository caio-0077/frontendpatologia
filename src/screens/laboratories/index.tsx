import { useState, useEffect, ChangeEvent } from 'react';
import {
  LaboratoriesHeader,
  ModalToAddLaboratorie,
  ModalImportLaboratorie,
  ModalToEditLaboratorie,
} from './components';
import { formatCNPJ } from 'utils/converters';
import { IconButton, Checkbox } from '@mui/material';
import { Table, SortButton, Pagination, Icon } from 'components';
import { useLaboratories } from 'hooks/use-laboratories';

const PAGE_SIZE = 10;

type ObjectType = {
  [key: string]: string | number | boolean | undefined;
};

export const Laboratories = () => {
  const [search, setSearch] = useState('');
  const [isModalToAdd, setIsModalToAdd] = useState(false);
  const [isModalToEdit, setIsModalToEdit] = useState(false);
  const [isModalImport, setIsModalImport] = useState(false);
  const [idLaboratorie, setIdLaboratorie] = useState('');
  const [filter, setFilter] = useState({
    page: 1,
    search: '',
    orderDesc: 0,
    pageSize: PAGE_SIZE,
    sortField: 'company_name',
  });

  const { getLaboratories, laboratories, isLaboratoriesLoading } =
    useLaboratories();

  useEffect(() => {
    const debounceInput = setTimeout(
      () => {
        setFilter((old) => ({ ...old, search, page: 1 }));
      },
      search ? 1000 : 0,
    );

    return () => clearTimeout(debounceInput);
  }, [search]);

  useEffect(() => {
    getLaboratories(filter);
  }, [getLaboratories, filter]);

  const handleAddLaboratorie = () => {
    setIsModalToAdd(true);
  };

  const handleEditLaboratorie = (id: string | undefined) => {
    if (id) setIdLaboratorie(id);
    setIsModalToEdit(true);
  };

  const handleImportLaboratorie = () => {
    setIsModalImport(true);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<{ value: unknown }>) => {
    const newPageSize = event.target.value as number;
    setFilter((old) => ({
      ...old,
      pageSize: newPageSize || PAGE_SIZE,
      page: 1,
    }));
  };

  const handleChangePage = (
    _: ChangeEvent<unknown> | null,
    newPage: number,
  ) => {
    setFilter((old) => ({ ...old, page: newPage }));
  };

  const onUpFunction = () => {
    setFilter((old) => ({
      ...old,
      page: 1,
      orderDesc: 1,
      sortField: 'company_name',
    }));
  };

  const onDownFunction = () => {
    setFilter((old) => ({
      ...old,
      page: 1,
      orderDesc: 0,
      sortField: 'company_name',
    }));
  };

  const onRefresh = () => [
    getLaboratories({ page: 1, pageSize: 10, orderDesc: 0 }),
  ];

  const columns = [
    {
      field: 'id',
      width: 'w-0',
      label: <Checkbox disabled />,
      render: () => <Checkbox disabled />,
    },
    {
      field: 'company_name',
      label: (
        <div className="flex flex-row gap-1 items-center">
          <span>Razão social</span>
          <SortButton
            onUpFunction={onUpFunction}
            onDownFunction={onDownFunction}
          />
        </div>
      ),
    },
    {
      field: 'cnpj',
      label: 'CNPJ',
      render: ({ cnpj }: ObjectType) => formatCNPJ(`${cnpj}`),
    },
    { field: 'size', label: 'Porte' },
    {
      field: 'status',
      label: 'Status',
      render: () => (
        <div className="bg-tertiary-30 bg-opacity-20 justify-center items-center flex rounded-full py-1 w-24">
          <span className="font-medium text-sm text-tertiary-30">Ativo</span>
        </div>
      ),
    },
    {
      field: 'edit',
      width: 'w-0',
      label: '',
      render: ({ id }: ObjectType) => (
        <IconButton onClick={() => handleEditLaboratorie(`${id}`)}>
          <Icon name="edit" size="small" className="text-neutral_variant-30" />
        </IconButton>
      ),
    },
  ];

  return (
    <div className="flex items-start justify-start w-full h-screen px-6 py-10 flex-col overflow-x-auto">
      <div className="w-full flex flex-col gap-6">
        <LaboratoriesHeader
          search={search}
          setSearch={setSearch}
          handleAddLaboratorie={handleAddLaboratorie}
          handleImportLaboratorie={handleImportLaboratorie}
        />
        <Table
          columns={columns}
          isLoading={isLaboratoriesLoading}
          data={laboratories?.content}
          messageEmptyStatus="Ajuste sua busca ou adicione o laboratório"
          pagination={
            <Pagination
              count={Math.ceil(
                (laboratories?.pagination?.total || 0) / filter.pageSize,
              )}
              page={filter.page}
              rowsPerPage={filter.pageSize}
              changePerPage={handleChangeRowsPerPage}
              onChange={handleChangePage}
            />
          }
        />
      </div>
      <ModalToAddLaboratorie
        open={isModalToAdd}
        onRefresh={onRefresh}
        onClose={() => setIsModalToAdd(false)}
      />
      <ModalToEditLaboratorie
        open={isModalToEdit}
        idLaboratorie={idLaboratorie}
        onRefresh={onRefresh}
        setIdLaboratorie={setIdLaboratorie}
        onClose={() => setIsModalToEdit(false)}
      />
      <ModalImportLaboratorie
        open={isModalImport}
        onRefresh={onRefresh}
        onClose={() => setIsModalImport(false)}
      />
    </div>
  );
};
