import { FC } from 'react';
import { FormElements } from 'components';
import { Controller, Control, FieldErrors } from 'react-hook-form';

type FormValues = {
  companyName: string;
  tradeName?: string | undefined;
  cnpj: string;
  street?: string | undefined;
  phone?: string | undefined;
  size?: string | undefined;
};

interface RegisterFromProps {
  control: Control<FormValues>;
  errors: FieldErrors;
}

const sizeOptions = [
  {
    id: 1,
    label: 'Pequeno',
    value: 'pequeno',
  },
  {
    id: 2,
    label: 'Médio',
    value: 'medio',
  },
  {
    id: 3,
    label: 'Grande',
    value: 'grande',
  },
];

export const RegisterFrom: FC<RegisterFromProps> = ({ control, errors }) => {
  return (
    <div className="w-full flex flex-col gap-6">
      <Controller
        control={control}
        name="companyName"
        defaultValue=""
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            shrink
            errors={errors}
            label="Razão social*"
            name={name}
            value={value}
            variant="standard"
            onChange={onChange}
          />
        )}
      />
      <Controller
        control={control}
        name="tradeName"
        defaultValue=""
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Input
            shrink
            errors={errors}
            label="Nome fantasia"
            name={name}
            value={value}
            variant="standard"
            onChange={onChange}
          />
        )}
      />
      <Controller
        control={control}
        name="size"
        defaultValue="medio"
        render={({ field: { onChange, value, name } }) => (
          <FormElements.Select
            shrink
            errors={errors}
            label="Porte"
            name={name}
            value={value}
            variant="standard"
            onChange={onChange}
            options={sizeOptions}
          />
        )}
      />
      <Controller
        control={control}
        name="cnpj"
        defaultValue=""
        render={({ field: { onChange, value, name } }) => (
          <FormElements.InputMask
            shrink
            errors={errors}
            label="CNPJ*"
            mask="cnpj"
            name={name}
            value={value}
            variant="standard"
            onChange={onChange}
          />
        )}
      />
      <div className="w-full flex flex-row gap-6">
        <div className="w-full">
          <Controller
            control={control}
            name="street"
            defaultValue=""
            render={({ field: { onChange, value, name } }) => (
              <FormElements.Input
                shrink
                errors={errors}
                label="Endereço"
                name={name}
                value={value}
                variant="standard"
                onChange={onChange}
              />
            )}
          />
        </div>
        <div className="w-full">
          <Controller
            control={control}
            name="phone"
            defaultValue=""
            render={({ field: { onChange, value, name } }) => (
              <FormElements.InputMask
                shrink
                label="Telefone"
                mask="phone"
                name={name}
                value={value}
                variant="standard"
                onChange={onChange}
              />
            )}
          />
        </div>
      </div>
    </div>
  );
};
