import { FC, useEffect } from 'react';
import { Modal } from 'components';
import { registerValidation } from '../validation';
import { removePointsCNPJ } from 'utils/converters';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { FormModalsLaboratorie } from '../form-modals-laboratorie';
import { useLaboratories } from 'hooks/use-laboratories';

type FormValues = {
  companyName: string;
  tradeName?: string | undefined;
  cnpj: string;
  street?: string | undefined;
  phone?: string | undefined;
  size?: string | undefined;
};

interface ModalToAddLaboratorieProps {
  open: boolean;
  onClose: () => void;
  onRefresh: () => void;
}

export const ModalToAddLaboratorie: FC<ModalToAddLaboratorieProps> = ({
  open,
  onClose,
  onRefresh,
}) => {
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormValues>({
    resolver: yupResolver(registerValidation),
  });
  const { createLaboratorie, isCreateLaboratorieLoading } = useLaboratories();

  useEffect(() => {
    reset();
  }, [open, reset]);

  const onSubmit: SubmitHandler<FormValues> = async (data) => {
    try {
      const dataFormat = {
        companyName: data?.companyName,
        tradeName: data?.tradeName,
        cnpj: removePointsCNPJ(data?.cnpj),
        size: data?.size,
        street: data?.street,
        phone: data?.phone,
      };
      await createLaboratorie(dataFormat);
      onRefresh();
      onClose();
    } catch (error) {
      throw new Error();
    }
  };

  return (
    <Modal
      onSubmit={handleSubmit(onSubmit)}
      title="Adicionar laboratório"
      subTitle="Preencha os dados"
      hasButtonCancel={false}
      textButton="Adicionar"
      open={open}
      onClose={onClose}
      isLoading={isCreateLaboratorieLoading}
    >
      <FormModalsLaboratorie.Register control={control} errors={errors} />
    </Modal>
  );
};
