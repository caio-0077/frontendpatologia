import * as yup from 'yup';

const registerValidation = yup.object().shape({
  companyName: yup.string().required('Razão social é obrigatória'),
  cnpj: yup.string().required('CNPJ é obrigatório'),
});

export { registerValidation };
