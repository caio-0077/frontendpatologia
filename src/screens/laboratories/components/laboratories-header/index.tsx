import { FC } from 'react';
import { FormElements, Button } from 'components';

interface LaboratoriesHeaderProps {
  search: string;
  setSearch: (_search: string) => void;
  handleAddLaboratorie: () => void;
  handleImportLaboratorie: () => void;
}

export const LaboratoriesHeader: FC<LaboratoriesHeaderProps> = ({
  search,
  setSearch,
  handleAddLaboratorie,
  handleImportLaboratorie,
}) => {
  return (
    <div className="flex flex-col w-full gap-8">
      <div>
        <span className="text-2xl text-primary-20">Laboratórios</span>
      </div>
      <div className="flex flex-row w-full justify-between items-center p-4 shadow-md bg-white rounded-lg">
        <div className="w-[320px]">
          <FormElements.Input
            search
            isClean
            value={search}
            variant="standard"
            onChange={setSearch}
            label="Razão social ou CNPJ"
            placeholder="Razão social ou CNPJ"
          />
        </div>
        <div className="flex flex-row gap-6">
          <Button icon="plus" variant="outlined" onClick={handleAddLaboratorie}>
            Adicionar
          </Button>
          <Button icon="upload" onClick={handleImportLaboratorie}>
            Importar CSV
          </Button>
        </div>
      </div>
    </div>
  );
};
