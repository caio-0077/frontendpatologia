import { FC, useState, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import { Modal, Icon } from 'components';
import { formatSizeUnits } from 'utils/converters';
import { useLaboratories } from 'hooks/use-laboratories';

interface ModalImportLaboratorieProps {
  open: boolean;
  onClose: () => void;
  onRefresh: () => void;
}

interface CSVType {
  name?: string;
  file?: File | Blob | undefined;
  size?: string;
  error?: boolean;
}

export const ModalImportLaboratorie: FC<ModalImportLaboratorieProps> = ({
  open,
  onClose,
  onRefresh,
}) => {
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone();
  const [csv, setCsv] = useState<CSVType>({
    error: false,
  });
  const { sendCSV, isSendCSVLoading } = useLaboratories();

  useEffect(() => {
    setCsv({
      name: '',
      size: '',
      file: undefined,
      error: false,
    });
  }, [open]);

  useEffect(() => {
    const insertCsv = async () => {
      if (acceptedFiles?.length > 0) {
        const file = acceptedFiles[0];
        if (
          file &&
          (file.type === 'text/csv' || file.type === 'application/csv')
        ) {
          const reader = new FileReader();
          setCsv({
            name: file?.name,
            size: formatSizeUnits(file?.size),
            file: file,
            error: false,
          });
          reader.readAsText(acceptedFiles[0]);
        } else {
          return setCsv({
            name: '',
            size: '',
            file: undefined,
            error: true,
          });
        }
      }
    };
    insertCsv();
  }, [acceptedFiles]);

  const handleSendCSV = async () => {
    try {
      if (csv?.name) {
        await sendCSV(csv);
      } else {
        return setCsv({
          name: '',
          size: '',
          file: undefined,
          error: true,
        });
      }
      setCsv({
        name: '',
        size: '',
        file: undefined,
        error: false,
      });
      onRefresh();
      onClose();
    } catch (error) {
      throw new Error();
    }
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      textButton="Importar"
      title="Importar CSV"
      onClick={handleSendCSV}
      subTitle="Importe o arquivo"
      hasButtonCancel={false}
      isLoading={isSendCSVLoading}
    >
      <div className="flex flex-col gap-4 justify-center items-center">
        {!!csv?.name && !csv.error && (
          <div className="h-52 w-full border-dashed border border-white-30 rounded-lg justify-center items-start flex gap-2 flex-row p-6">
            <div>
              <div className="p-2 border border-solid rounded border-neutral_variant-60">
                <Icon name="file" className="text-primary-40" />
              </div>
            </div>
            <div className="w-full">
              <div className="flex flex-row justify-between w-full items-start">
                <div className="flex flex-col">
                  <span className="text-neutral-10 text-sm">{csv.name}</span>
                  <span className="text-xs text-neutral_variant-50 font-medium">
                    197kB
                  </span>
                </div>
                <button
                  className="hover:opacity-50"
                  onClick={() =>
                    setCsv({
                      name: '',
                      size: '',
                      file: undefined,
                      error: false,
                    })
                  }
                >
                  <Icon name="close" className="text-neutral_variant-30" />
                </button>
              </div>
              <div className="w-full flex flex-row justify-center items-center font-medium gap-2">
                <div className="h-2 rounded-2xl bg-primary-40 w-full" />
                <span className="text-xs text-neutral-30">100%</span>
              </div>
            </div>
          </div>
        )}
        {!csv?.name && !csv.error && (
          <div
            {...getRootProps({ className: 'dropzone' })}
            className="h-52 w-full border-dashed border border-white-30 rounded-lg justify-center items-center flex gap-2 flex-col cursor-pointer hover:opacity-50"
          >
            <input {...getInputProps()} />
            <Icon name="file" size="large" className="text-white-20" />
            <span className="text-xl text-neutral_variant-30">
              Clique e arraste o arquivo aqui
            </span>
          </div>
        )}
        {csv?.error && (
          <div
            {...getRootProps({ className: 'dropzone' })}
            className="h-52 w-full border-dashed border border-white-30 rounded-lg justify-center items-center flex gap-2 flex-col cursor-pointer hover:opacity-50"
          >
            <input {...getInputProps()} />
            <Icon name="report-error" size="large" className="text-error-30" />
            <span className="text-xl text-neutral_variant-30">
              Arquivo não suportado
            </span>
            <span className="text-sm text-neutral_variant-80">
              Suba somente arquivos CSV no modelo indicado acima
            </span>
          </div>
        )}
      </div>
    </Modal>
  );
};
