import { FC, useEffect, Dispatch, SetStateAction } from 'react';
import { Modal } from 'components';
import { registerValidation } from '../validation';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, SubmitHandler } from 'react-hook-form';
import { FormModalsLaboratorie } from '../form-modals-laboratorie';
import { useLaboratories } from 'hooks/use-laboratories';
import { removePointsCNPJ, formatCNPJ } from 'utils/converters';

type FormValues = {
  companyName: string;
  tradeName?: string | undefined;
  cnpj: string;
  street?: string | undefined;
  phone?: string | undefined;
  size?: string | undefined;
};

interface ModalToEditLaboratorieProps {
  open: boolean;
  onClose: () => void;
  idLaboratorie?: string;
  onRefresh: () => void;
  setIdLaboratorie: Dispatch<SetStateAction<string>>;
}

export const ModalToEditLaboratorie: FC<ModalToEditLaboratorieProps> = ({
  open,
  onClose,
  idLaboratorie,
  setIdLaboratorie,
  onRefresh,
}) => {
  const {
    control,
    setValue,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormValues>({
    resolver: yupResolver(registerValidation),
  });
  const {
    updateLaboratorie,
    isUpdateLaboratorieLoading,
    getLaboratorie,
    laboratorie,
  } = useLaboratories();

  useEffect(() => {
    reset();
    setIdLaboratorie('');
  }, [open, reset, setIdLaboratorie]);

  useEffect(() => {
    if (idLaboratorie) getLaboratorie(idLaboratorie);
  }, [getLaboratorie, idLaboratorie]);

  useEffect(() => {
    if (laboratorie.id) {
      if (laboratorie.company_name)
        setValue('companyName', laboratorie.company_name);
      setValue('tradeName', laboratorie.trade_name);
      if (laboratorie.cnpj) setValue('cnpj', formatCNPJ(laboratorie.cnpj));
      setValue('size', laboratorie.size);
      setValue('street', laboratorie.street);
      setValue('phone', laboratorie.phone);
    }
  }, [setValue, laboratorie, idLaboratorie]);

  const onSubmit: SubmitHandler<FormValues> = async (data) => {
    try {
      const dataFormat = {
        companyName: data?.companyName,
        tradeName: data?.tradeName,
        cnpj: removePointsCNPJ(data?.cnpj),
        size: data?.size,
        street: data?.street,
        phone: data?.phone,
      };
      await updateLaboratorie({ id: laboratorie?.id, data: dataFormat });
      onRefresh();
      setIdLaboratorie('');
      onClose();
    } catch (error) {
      throw new Error();
    }
  };

  return (
    <Modal
      onSubmit={handleSubmit(onSubmit)}
      title="Editar laboratório"
      open={open}
      onClose={onClose}
      isLoading={isUpdateLaboratorieLoading}
    >
      <FormModalsLaboratorie.Register control={control} errors={errors} />
    </Modal>
  );
};
