import { api } from 'services';

const URL_LOGIN = '/v1/session';
const URL_FORGOT = '/v1/password/forgot';
const URL_RESET = '/v1/password/reset';

type DataSignIn = {
  token: string;
  user: {
    [key: string]: undefined;
  };
};

interface SignInProps {
  email: string;
  password: string;
}

interface ForgotPasswordProps {
  email: string;
}

interface ResetPasswordProps {
  token?: string;
  password: string;
}

interface SignInResponse {
  data: DataSignIn;
}

export const auth = {
  signIn: async (data: SignInProps): Promise<SignInResponse> => {
    return await api.post(URL_LOGIN, data);
  },
  validate: async () => {
    return await api.get(URL_LOGIN);
  },
  forgotPassword: async (data: ForgotPasswordProps) => {
    return await api.post(URL_FORGOT, data);
  },
  resetPassword: async (data: ResetPasswordProps) => {
    return await api.post(URL_RESET, data);
  },
};
