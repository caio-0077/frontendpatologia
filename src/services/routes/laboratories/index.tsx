import { api } from 'services';

const URL_LAB = '/v1/labs';

interface GetLabsProps {
  page?: number;
  search?: string;
  orderDesc?: number;
  pageSize?: number;
  sortField?: string;
}

interface GetLabProps {
  id?: string;
}

interface CreateLabProps {
  companyName?: string;
  tradeName?: string;
  cnpj?: string;
  size?: string;
  street?: string;
  number?: string;
  district?: string;
  city?: string;
  zipCode?: string;
  phone?: string;
  referencePoint?: string;
}

interface UpdateLabProps {
  id?: string;
  data: {
    companyName?: string;
    tradeName?: string;
    cnpj?: string;
    size?: string;
    street?: string;
    number?: string;
    district?: string;
    city?: string;
    zipCode?: string;
    phone?: string;
    referencePoint?: string;
  };
}

interface DeleteLabProps {
  id?: string;
}

interface GetLabResponse {
  data: {
    id?: string;
    company_name?: string;
    trade_name?: string;
    cnpj?: string;
    size?: string;
    street?: string;
    number?: string;
    district?: string;
    city?: string;
    zip_code?: string;
    reference_point?: string;
    status?: string;
    phone?: string;
    created_at?: string;
    updated_at?: string;
  };
}

interface GetLabsResponse {
  data: {
    content: [
      {
        id?: string;
        company_name?: string;
        trade_name?: string;
        cnpj?: string;
        size?: string;
        street?: string;
        number?: string;
        city?: string;
        zip_code?: string;
        reference_point?: string;
        phone?: string;
        created_at?: string;
        updated_at?: string;
      },
    ];
    pagination: {
      page?: number;
      pageSize?: number;
      total?: number;
    };
  };
}

interface SendCSVProps {
  name?: string;
  file?: File | Blob | undefined;
  size?: string;
  error?: boolean;
}

export const laboratories = {
  getLabs: async (filters: GetLabsProps): Promise<GetLabsResponse> => {
    return await api.get(URL_LAB, {
      params: filters || null,
    });
  },

  getLab: async ({ id }: GetLabProps): Promise<GetLabResponse> => {
    return await api.get(`${URL_LAB}/${id}`);
  },

  createLab: async (data: CreateLabProps) => {
    return await api.post(URL_LAB, data);
  },

  updateLab: async ({ data, id }: UpdateLabProps) => {
    return await api.put(`${URL_LAB}/${id}`, data);
  },

  deleteLab: async ({ id }: DeleteLabProps) => {
    return await api.delete(`${URL_LAB}/${id}`);
  },

  sendCSV: async (data: SendCSVProps) => {
    if (!data.file) return;
    const formData = new FormData();
    formData.append('file', data.file);
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    };
    return await api.patch(`${URL_LAB}/csv`, formData, config);
  },
};
