import { auth } from './auth';
import { laboratories } from './laboratories';

export const routesApi = {
  auth: auth,
  laboratories: laboratories,
};
