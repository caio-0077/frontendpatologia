import axios, { AxiosError } from 'axios';
import config from './config';
import { storage } from 'utils/local-storage';

const BASE_URL = config.url.api.admin;
const api = axios.create({
  baseURL: BASE_URL,
});

api.interceptors.request.use((config) => {
  const token = window.localStorage.getItem(storage.token);

  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }
  return config;
});

api.interceptors.response.use(
  (value) => {
    return Promise.resolve(value);
  },
  (error: AxiosError) => {
    const { isAxiosError = false, response = null } = error;

    if (isAxiosError && response) {
      const { status } = response;

      if (status === 401 || status === 403) {
        return Promise.reject(error);
      }
    }

    return Promise.reject(error);
  },
);

export { api };
