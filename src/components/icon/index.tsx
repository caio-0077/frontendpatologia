import React from 'react';
import AddIcon from '@mui/icons-material/Add';
import InfoIcon from '@mui/icons-material/Info';
import EditIcon from '@mui/icons-material/Edit';
import CloseIcon from '@mui/icons-material/Close';
import SearchIcon from '@mui/icons-material/Search';
import DownloadIcon from '@mui/icons-material/Download';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import VerifiedOutlinedIcon from '@mui/icons-material/VerifiedOutlined';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import TableChartOutlinedIcon from '@mui/icons-material/TableChartOutlined';
import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';

interface IconProps {
  name: string;
  className?: string;
  size?: 'inherit' | 'large' | 'medium' | 'small';
}

export const Icon: React.FC<IconProps> = ({
  name,
  className,
  size = 'medium',
}) => {
  const Icons = {
    plus: <AddIcon fontSize={size} className={className} />,
    info: <InfoIcon fontSize={size} className={className} />,
    edit: <EditIcon fontSize={size} className={className} />,
    close: <CloseIcon fontSize={size} className={className} />,
    search: <SearchIcon fontSize={size} className={className} />,
    download: <DownloadIcon fontSize={size} className={className} />,
    upload: <FileUploadIcon fontSize={size} className={className} />,
    file: <InsertDriveFileIcon fontSize={size} className={className} />,
    'chart-line': <ShowChartIcon fontSize={size} className={className} />,
    eye: <VisibilityOutlinedIcon fontSize={size} className={className} />,
    table: <TableChartOutlinedIcon fontSize={size} className={className} />,
    verified: <VerifiedOutlinedIcon fontSize={size} className={className} />,
    'arrow-up-ward': <ArrowUpwardIcon fontSize={size} className={className} />,
    'arrow-drop-up': <ArrowDropUpIcon fontSize={size} className={className} />,
    'close-circle': <HighlightOffIcon fontSize={size} className={className} />,
    eyeOff: <VisibilityOffOutlinedIcon fontSize={size} className={className} />,
    'report-error': (
      <ReportGmailerrorredIcon fontSize={size} className={className} />
    ),
    'arrow-down-ward': (
      <ArrowDownwardIcon fontSize={size} className={className} />
    ),
    'arrow-drop-down': (
      <ArrowDropDownIcon fontSize={size} className={className} />
    ),
  }[name];
  return Icons || null;
};
