import { Icon } from 'components/icon';
import logoSBP from 'assets/sbp-logo.png';
import { useDrawerMenu } from 'hooks/contexts/use-drawer-menu';

export const DrawerHeader = () => {
  const { hasOpenOrClosedDrawer, openDrawerMenu, closeDrawerMenu } =
    useDrawerMenu();

  return (
    <div
      className={`flex flex-row items-center justify-center w-full pr-6 ${
        hasOpenOrClosedDrawer && 'items-start justify-between flex'
      }`}
    >
      {hasOpenOrClosedDrawer && (
        <>
          <img src={logoSBP} alt="spb-logo" className="w-[133px] h-[56px]" />
          <button onClick={closeDrawerMenu} className="hover:opacity-80">
            <Icon name="close" className="text-white" />
          </button>
        </>
      )}
      {!hasOpenOrClosedDrawer && (
        <button
          onClick={openDrawerMenu}
          className="flex items-center justify-center hover:opacity-80"
        >
          <Icon name="plus" className="text-white" />
        </button>
      )}
    </div>
  );
};
