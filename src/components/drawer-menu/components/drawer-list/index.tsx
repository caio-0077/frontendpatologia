import { Icon } from 'components/icon';
import { useDrawerMenu } from 'hooks/contexts/use-drawer-menu';

export const DrawerList = () => {
  const { hasOpenOrClosedDrawer } = useDrawerMenu();

  return (
    <div className="flex flex-col gap-8">
      <div>
        {/* {hasOpenOrClosedDrawer && (
          <div className="flex flex-row items-center justify-between w-full p-2 pr-6">
            <span className="text-sm font-medium text-white">Indicadores</span>
            <button className="hover:opacity-80">
              <Icon name="arrow-drop-down" className="text-white" />
            </button>
          </div>
        )} */}
        <div className="flex flex-row p-2 bg-white cursor-pointer rounded-s-lg">
          <div className="flex flex-row items-center gap-6">
            <Icon name="table" className="text-primary-40" />
            {hasOpenOrClosedDrawer && (
              <span className="text-sm font-medium text-primary-20">
                Laboratórios
              </span>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
