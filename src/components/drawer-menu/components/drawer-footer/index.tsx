import { colors } from 'utils/colors';
import { Avatar } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useAuth } from 'hooks/contexts/use-auth';
import { useDrawerMenu } from 'hooks/contexts/use-drawer-menu';

const styleAvatar = {
  bgcolor: colors.primary[40],
  width: '50px',
  height: '50px',
  fontSize: '24px',
};

export const DrawerFooter = () => {
  const navigate = useNavigate();
  const { signOut, user } = useAuth();

  const { hasOpenOrClosedDrawer } = useDrawerMenu();

  const name = user?.name || 'user';
  const twoFirstLetters = name.substring(0, 2).toUpperCase();

  const handleSignOut = async () => {
    try {
      await signOut();
      navigate('/login');
    } catch (error) {
      throw new Error();
    }
  };

  return (
    <div
      className={`flex flex-row items-start justify-start gap-6 pr-6 ${
        !hasOpenOrClosedDrawer && 'items-center '
      }`}
    >
      <Avatar sx={styleAvatar}>{twoFirstLetters}</Avatar>
      {hasOpenOrClosedDrawer && (
        <div className="flex flex-col items-start gap-1">
          <span className="text-sm font-medium text-white">{user?.name}</span>
          {user?.type !== 'admin' && (
            <span className="text-sm font-medium text-white-10 capitalize">
              {user?.type}
            </span>
          )}
          <button className="hover:opacity-80" onClick={handleSignOut}>
            <span className="text-sm font-medium text-error-80">Sair</span>
          </button>
        </div>
      )}
    </div>
  );
};
