import Drawer from '@mui/material/Drawer';
import {
  DrawerList,
  DrawerTitle,
  DrawerHeader,
  DrawerFooter,
} from './components';
import { colors } from 'utils/colors';
import { useDrawerMenu } from 'hooks/contexts/use-drawer-menu';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const drawerWidth = '260px';

export const DrawerMenu = () => {
  const theme = createTheme();
  const { hasOpenOrClosedDrawer } = useDrawerMenu();

  const styles = {
    drawerOpen: {
      '& .MuiDrawer-paper': {
        display: 'flex',
        position: 'relative !important',
        justifyContent: 'space-between',
        padding: '24px 0px 24px 24px',
        alignItems: 'flex-center',
        background: colors.primary[20],
        maxWidth: '280px',
        width: drawerWidth,
        border: '0px solid transparent',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
    },
    drawerClosed: {
      '& .MuiDrawer-paper': {
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9) + 1,
        },
        display: 'flex',
        position: 'relative !important',
        justifyContent: 'space-between',
        padding: '24px 0px 24px 24px',
        alignItems: 'flex-center',
        background: colors.primary[20],
        border: '0px solid transparent',
      },
    },
  };

  const drawerStyle = hasOpenOrClosedDrawer
    ? styles.drawerOpen
    : styles.drawerClosed;

  return (
    <ThemeProvider theme={theme}>
      <Drawer sx={drawerStyle} variant="permanent" open={hasOpenOrClosedDrawer}>
        <div className="flex flex-col gap-8">
          <DrawerHeader />
          {hasOpenOrClosedDrawer && <DrawerTitle />}
          <DrawerList />
        </div>
        <DrawerFooter />
      </Drawer>
    </ThemeProvider>
  );
};
