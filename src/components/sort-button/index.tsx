import { FC, useState, CSSProperties } from 'react';
import { Icon } from 'components/icon';
import { IconButton } from '@mui/material';

interface SortButtonProps {
  onUpFunction?: () => void;
  onDownFunction?: () => void;
}

const buttonStyle: CSSProperties = { padding: 0.5 };

export const SortButton: FC<SortButtonProps> = ({
  onUpFunction,
  onDownFunction,
}) => {
  const [isOrdination, setIsOrdination] = useState<boolean>(false);

  const handleOrdination = () => {
    if (isOrdination) {
      if (onDownFunction) onDownFunction();
    } else {
      if (onUpFunction) onUpFunction();
    }
    setIsOrdination((old) => !old);
  };

  return (
    <IconButton onClick={handleOrdination} sx={buttonStyle}>
      {isOrdination ? (
        <Icon
          size="small"
          name="arrow-down-ward"
          className="text-neutral_variant-30"
        />
      ) : (
        <Icon
          size="small"
          name="arrow-up-ward"
          className="text-neutral_variant-30"
        />
      )}
    </IconButton>
  );
};
