import React from 'react';

import { buttonStyle, disabledStyle } from './styles';
import { Icon } from 'components/icon';
import CircularProgress from '@mui/material/CircularProgress';

interface ButtonProps {
  icon?: string;
  width?: string;
  widthFull?: boolean;
  disabled?: boolean;
  isLoading?: boolean;
  onClick?: () => void;
  type?: 'button' | 'submit';
  children?: React.ReactNode;
  variant?: 'filled' | 'outlined' | 'text' | 'elevated' | 'tonal';
}

export const Button: React.FC<ButtonProps> = ({
  width,
  onClick,
  children,
  icon = '',
  type = 'button',
  disabled = false,
  widthFull = false,
  isLoading = false,
  variant = 'filled',
}) => {
  const disabledClass = `${(isLoading || disabled) && disabledStyle[variant]}`;
  const widthClass = `${widthFull && 'w-full'} ${width}`;
  const buttonClass = `${buttonStyle[variant]} ${widthClass} ${disabledClass}`;

  return (
    <div>
      <button
        type={type}
        className={buttonClass}
        onClick={onClick}
        disabled={isLoading || disabled}
      >
        {isLoading && <CircularProgress size={18} style={{ color: '#FFF' }} />}
        {!isLoading && (
          <>
            {!!icon && <Icon name={icon} size="small" />}
            {children}
          </>
        )}
      </button>
    </div>
  );
};
