const defaultButtonStyle =
  'h-10 rounded-full px-6 py-2.5 gap-2 justify-center items-center cursor-pointer flex font-medium text-sm min-w-[83px]';

const darkStyle = {
  filled: 'dark:bg-primary-90 dark:text-primary-20',
  outlined:
    'dark:text-primary-90 dark:border-neutral_variant-60 dark:hover:bg-hover-10',
  text: 'dark:text-primary-90 dark:hover:bg-hover-10',
  elevated: 'dark:text-primary-90 dark:hover:bg-hover-10',
  tonal: 'dark:bg-secondary-30 dark:text-secondary-90 dark:hover:opacity-90',
};

export const disabledStyle = {
  filled:
    'bg-disabled-10 shadow-none text-disabled-0 opacity-60 dark:bg-disabled-20 dark:text-disabled-30 cursor-not-allowed',
  outlined:
    'border-disabled-10 shadow-none text-disabled-0 opacity-50  dark:border-disabled-20 dark:text-disabled-30 cursor-not-allowed',
  text: 'shadow-none text-disabled-0 opacity-50  dark:text-disabled-30 cursor-not-allowed',
  elevated:
    'bg-disabled-10 shadow-none text-disabled-0 opacity-60 dark:bg-disabled-20 dark:text-disabled-30 cursor-not-allowed',
  tonal:
    'bg-disabled-10 shadow-none text-disabled-0 opacity-60  dark:bg-disabled-20 dark:text-disabled-30 cursor-not-allowed',
};

export const buttonStyle = {
  filled: `${defaultButtonStyle} bg-primary-40 text-white shadow-md enabled:hover:opacity-90 ${darkStyle['filled']}`,
  outlined: `${defaultButtonStyle} border-solid border-neutral-50 border text-primary-40 bg-transparent enabled:hover:bg-hover-0 ${darkStyle['outlined']}`,
  text: `${defaultButtonStyle} text-primary-40 bg-transparent enabled:hover:bg-hover-0 ${darkStyle['text']}`,
  elevated: `${defaultButtonStyle} text-primary-40 bg-transparent shadow-md hover:bg-hover-0 ${darkStyle['elevated']}`,
  tonal: `${defaultButtonStyle} text-secondary-10 bg-secondary-90 enabled:hover:opacity-80 ${darkStyle['tonal']}`,
};
