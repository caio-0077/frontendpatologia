import { ChangeEvent, FC } from 'react';
import { Pagination as PaginationMUI } from '@mui/material';
import { styled } from '@mui/system';
import { colors } from 'utils/colors';

interface PaginationProps {
  page?: number;
  onChange?: (_event: ChangeEvent<unknown>, _page: number) => void;
  rowsPerPage?: number;
  count: number;
  changePerPage?: (_event: ChangeEvent<HTMLSelectElement>) => void;
  rowsPerPagePersonality?: number[];
}

const CustomizedPagination = styled(PaginationMUI)`
  .MuiPaginationItem-root.Mui-selected {
    background-color: ${colors.primary[20]};
    color: #fff;
  }

  .MuiPaginationItem-page:hover {
    background-color: rgba(103, 80, 164, 0.08);
  }
`;

export const Pagination: FC<PaginationProps> = ({
  page,
  onChange,
  rowsPerPage,
  count,
  changePerPage,
  rowsPerPagePersonality = [10, 15, 20, 30],
}) => {
  return (
    <div className="flex flex-row gap-3 items-center justify-center xs:flex-col">
      <CustomizedPagination
        page={page}
        shape="rounded"
        defaultPage={1}
        siblingCount={1}
        onChange={onChange}
        count={Math.ceil(count)}
      />
      <div className="cursor-pointer">
        <select
          value={rowsPerPage}
          onChange={changePerPage}
          className="bg-white rounded-md shadow-sm focus:outline-none focus:ring-green-dark focus:border-green-dark sm:text-sm h-8 px-2"
        >
          {rowsPerPagePersonality.map((item, index) => (
            <option key={index} value={item}>
              {item}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
