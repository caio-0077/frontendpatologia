import { InputForm } from './input-form';
import { SelectForm } from './select-form';
import { InputMaskForm } from './input-mask-form';

export const FormElements = {
  Input: InputForm,
  Select: SelectForm,
  InputMask: InputMaskForm,
};
