import { FC } from 'react';
import { Icon } from 'components/icon';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';

interface RenderEndAdornmentProps {
  error?: boolean;
  value?: string | number;
  isClean?: boolean;
  password?: boolean;
  disabled?: boolean;
  isPassword?: boolean;
  handleClean?: () => void;
  handleClickShowPassword?: () => void;
}

interface PasswordAdornmentProps {
  disabled?: boolean;
  isPassword?: boolean;
  handleClickShowPassword?: () => void;
}

interface CloseAdornmentProps {
  disabled?: boolean;
  handleClean?: () => void;
}

const ErrorAdornment = () => {
  return (
    <InputAdornment position="end">
      <Icon
        name="info"
        size="medium"
        className="text-error-40 dark:text-error-80"
      />
    </InputAdornment>
  );
};

const PasswordAdornment: FC<PasswordAdornmentProps> = ({
  disabled,
  isPassword,
  handleClickShowPassword,
}) => {
  return (
    <InputAdornment position="end">
      <IconButton
        edge="end"
        disabled={disabled}
        onClick={handleClickShowPassword}
      >
        {isPassword ? (
          <Icon
            name="eyeOff"
            size="medium"
            className="text-neutral-30 dark:text-neutral_variant-80"
          />
        ) : (
          <Icon
            name="eye"
            size="medium"
            className="text-neutral-30 dark:text-neutral_variant-80"
          />
        )}
      </IconButton>
    </InputAdornment>
  );
};

const CloseAdornment: FC<CloseAdornmentProps> = ({ disabled, handleClean }) => {
  return (
    <InputAdornment position="end">
      <IconButton edge="end" disabled={disabled} onClick={handleClean}>
        <Icon
          name="close-circle"
          size="medium"
          className="text-neutral-30 dark:text-neutral_variant-80"
        />
      </IconButton>
    </InputAdornment>
  );
};

export const RenderEndAdornment: FC<RenderEndAdornmentProps> = ({
  error,
  value,
  isClean,
  password,
  disabled,
  isPassword,
  handleClean,
  handleClickShowPassword,
}) => {
  return (
    <>
      {error && <ErrorAdornment />}
      {isClean && !!value && !error && !password && (
        <CloseAdornment disabled={disabled} handleClean={handleClean} />
      )}
      {password && !error && (
        <PasswordAdornment
          disabled={disabled}
          isPassword={isPassword}
          handleClickShowPassword={handleClickShowPassword}
        />
      )}
    </>
  );
};
