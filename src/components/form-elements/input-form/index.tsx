import { useState, ChangeEvent, FC } from 'react';

import { TextField } from '../components';
import { FieldErrors } from 'react-hook-form';
import { getErrorMessageYup } from 'utils/converters';
import { RenderStartAdornment, RenderEndAdornment } from './components';

interface InputFormProps {
  name?: string;
  label?: string;
  value?: string | number;
  shrink?: boolean;
  search?: boolean;
  isClean?: boolean;
  password?: boolean;
  disabled?: boolean;
  errors?: FieldErrors;
  placeholder?: string;
  size?: 'small' | 'medium';
  onChange?: (_value: string) => void;
  callback?: (_value: string) => void;
  variant?: 'outlined' | 'standard' | 'filled';
}

export const InputForm: FC<InputFormProps> = ({
  name = '',
  size,
  label,
  value,
  errors,
  onChange,
  callback,
  placeholder,
  shrink = false,
  search = false,
  isClean = false,
  password = false,
  disabled = false,
  variant = 'outlined',
}) => {
  const helperText = getErrorMessageYup(errors, name);
  const error = !!helperText;

  const [isPassword, setIsPassword] = useState(false);

  const handleClean = () => {
    if (onChange) onChange('');
  };

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    if (onChange) onChange(newValue);
    if (callback) callback(newValue);
  };

  const handleClickShowPassword = () => {
    setIsPassword((show) => !show);
  };

  const type = password ? (isPassword ? 'text' : 'password') : 'text';

  return (
    <div className="w-full">
      <TextField
        fullWidth
        type={type}
        size={size}
        value={value}
        label={label}
        error={error}
        variant={variant}
        disabled={disabled}
        helperText={helperText}
        onChange={handleOnChange}
        placeholder={placeholder}
        InputLabelProps={shrink ? { shrink: true } : {}}
        autoComplete="off"
        className={`${disabled ? 'opacity-40' : 'opacity-100'} w-full`}
        InputProps={{
          autoComplete: 'off',
          startAdornment: search && <RenderStartAdornment />,
          endAdornment: (isClean || password) && (
            <RenderEndAdornment
              value={value}
              error={error}
              isClean={isClean}
              password={password}
              disabled={disabled}
              isPassword={isPassword}
              handleClean={handleClean}
              handleClickShowPassword={handleClickShowPassword}
            />
          ),
        }}
      />
    </div>
  );
};
