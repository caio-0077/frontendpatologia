import { colors } from 'utils/colors';
import { styled } from '@mui/material/styles';
import { TextField as TextFieldMUI } from '@mui/material';

export const TextField = styled(TextFieldMUI)(({ error, theme }) => ({
  '& .MuiFilledInput-root': {
    backgroundColor:
      theme.palette.mode === 'dark' ? '#323536' : colors.neutral[90],
    '&:before': {
      borderBottomColor:
        theme.palette.mode === 'dark' ? colors.primary[90] : colors.primary[40], // Substitua pelo valor da cor desejada
    },
  },
  '& .MuiFilledInput-root:after': {
    borderBottomColor:
      theme.palette.mode === 'dark' ? colors.primary[90] : colors.primary[40],
  },
  '& .MuiOutlinedInput-root': {
    color:
      theme.palette.mode === 'dark' ? colors.neutral[80] : colors.neutral[10],
    '& fieldset': {
      color:
        theme.palette.mode === 'dark'
          ? colors.neutral_variant[80]
          : colors.neutral_variant[30],
    },
    '&.Mui-focused fieldset': {
      borderColor:
        theme.palette.mode === 'dark' ? colors.primary[90] : colors.primary[40],
    },
  },
  '& .MuiInputLabel-root': {
    color:
      theme.palette.mode === 'dark'
        ? colors.neutral_variant[80]
        : colors.neutral_variant[30],
    '&.Mui-focused': {
      color:
        theme.palette.mode === 'dark' ? colors.primary[90] : colors.primary[40],
    },
    '& .MuiFormHelperText-root': {
      color:
        theme.palette.mode === 'dark'
          ? colors.neutral_variant[80]
          : colors.neutral_variant[30],
    },
  },
  ...(error && {
    '& .MuiFilledInput-root': {
      backgroundColor:
        theme.palette.mode === 'dark' ? '#323536' : colors.neutral[90],
      '&:before': {
        borderBottomWidth: '2px',
        borderBottomColor:
          theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40], // Substitua pelo valor da cor desejada
      },
    },
    '& .MuiFilledInput-root:after': {
      borderBottomColor:
        theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40],
    },
    '& .MuiOutlinedInput-root': {
      color:
        theme.palette.mode === 'dark' ? colors.neutral[80] : colors.neutral[10],
      '& fieldset': {
        borderColor: `${
          theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40]
        } !important`,
        borderWidth: '2px',
      },
      '&.Mui-focused fieldset': {
        borderColor:
          theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40],
      },
    },
    '& .MuiInputLabel-root': {
      color:
        theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40],
      '&.Mui-focused': {
        color:
          theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40],
      },
    },
    '& .MuiFormHelperText-root': {
      color:
        theme.palette.mode === 'dark' ? colors.error[80] : colors.error[40],
    },
  }),
}));
