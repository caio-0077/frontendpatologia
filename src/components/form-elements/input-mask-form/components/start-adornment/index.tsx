import { Icon } from 'components/icon';
import InputAdornment from '@mui/material/InputAdornment';

const SearchAdornment = () => {
  return (
    <InputAdornment position="end" className='pr-2'>
      <Icon
        name="search"
        size="medium"
        className="text-neutral-30 dark:text-neutral_variant-80"
      />
    </InputAdornment>
  );
};

export const RenderStartAdornment = () => {
  return <SearchAdornment />;
};
