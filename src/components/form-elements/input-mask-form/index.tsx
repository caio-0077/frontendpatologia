import { ChangeEvent, FC } from 'react';

import Input from 'react-input-mask';
import { TextField } from '../components';
import { FieldErrors } from 'react-hook-form';
import { getErrorMessageYup } from 'utils/converters';
import {
  maskTypes,
  RenderStartAdornment,
  RenderEndAdornment,
} from './components';

type Mask = 'cpf' | 'cnpj' | 'phone';

interface InputMaskFormProps {
  name?: string;
  mask: Mask;
  label?: string;
  shrink?: boolean;
  value?: string | number;
  search?: boolean;
  isClean?: boolean;
  disabled?: boolean;
  errors?: FieldErrors;
  placeholder?: string;
  size?: 'small' | 'medium';
  onChange?: (_value: string) => void;
  callback?: (_value: string) => void;
  variant?: 'outlined' | 'standard' | 'filled';
}

export const InputMaskForm: FC<InputMaskFormProps> = ({
  name = '',
  size,
  mask,
  label,
  value,
  errors,
  onChange,
  callback,
  placeholder,
  search = false,
  shrink = false,
  isClean = false,
  disabled = false,
  variant = 'outlined',
}) => {
  const helperText = getErrorMessageYup(errors, name);
  const error = !!helperText;

  const handleClean = () => {
    if (onChange) onChange('');
  };

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    if (onChange) onChange(newValue);
    if (callback) callback(newValue);
  };

  const maskType = maskTypes[mask];

  return (
    <div className="w-full">
      <Input
        mask={maskType}
        maskPlaceholder=""
        value={value}
        required={false}
        disabled={disabled}
        onChange={handleOnChange}
      >
        <div className="w-full">
          <TextField
            fullWidth
            type="text"
            size={size}
            label={label}
            error={error}
            variant={variant}
            helperText={helperText}
            placeholder={placeholder}
            InputLabelProps={shrink ? { shrink: true } : {}}
            className={`${disabled ? 'opacity-40' : 'opacity-100'} w-full`}
            InputProps={{
              autoComplete: 'off',
              startAdornment: search && <RenderStartAdornment />,
              endAdornment: isClean && (
                <RenderEndAdornment
                  value={value}
                  error={error}
                  isClean={isClean}
                  disabled={disabled}
                  handleClean={handleClean}
                />
              ),
            }}
          />
        </div>
      </Input>
    </div>
  );
};
