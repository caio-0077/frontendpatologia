import { FC, ChangeEvent } from 'react';

import { TextField } from '../components';
import { FieldErrors } from 'react-hook-form';
import { getErrorMessageYup } from 'utils/converters';
import MenuItem from '@mui/material/MenuItem';

type Options = {
  id: number;
  label: string;
  value: string;
}[];

interface InputFormProps {
  name?: string;
  label?: string;
  shrink?: boolean;
  options: Options;
  value?: string | number;
  disabled?: boolean;
  errors?: FieldErrors;
  placeholder?: string;
  size?: 'small' | 'medium';
  onChange?: (_value: string) => void;
  callback?: (_value: string) => void;
  variant?: 'outlined' | 'standard' | 'filled';
}

export const SelectForm: FC<InputFormProps> = ({
  name = '',
  size,
  label,
  value,
  errors,
  onChange,
  callback,
  options,
  placeholder,
  shrink = false,
  disabled = false,
  variant = 'outlined',
}) => {
  const helperText = getErrorMessageYup(errors, name);
  const error = !!helperText;

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    if (onChange) onChange(newValue);
    if (callback) callback(newValue);
  };

  return (
    <div className="w-full">
      <TextField
        fullWidth
        select
        type="text"
        size={size}
        value={value}
        label={label}
        error={error}
        variant={variant}
        disabled={disabled}
        helperText={helperText}
        onChange={handleOnChange}
        placeholder={placeholder}
        InputLabelProps={shrink ? { shrink: true } : {}}
        className={`${disabled ? 'opacity-40' : 'opacity-100'} w-full`}
      >
        {options.map((item) => (
          <MenuItem value={item.value} key={item.id}>
            {item.label}
          </MenuItem>
        ))}
      </TextField>
    </div>
  );
};
