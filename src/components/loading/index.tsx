import { colors } from 'utils/colors';
import CircularProgress from '@mui/material/CircularProgress';

export const Loading = () => {
  return (
    <div className="flex flex-row items-center justify-center w-full p-20">
      <div className="flex gap-5 p-5 bg-white rounded-md shadow-md">
        <span className="text-sm">Carregando</span>
        <CircularProgress style={{ color: colors.primary[20] }} size={20} />
      </div>
    </div>
  );
};
