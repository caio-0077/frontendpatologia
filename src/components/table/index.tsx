import { FC, ReactNode } from 'react';
import { Loading } from 'components';

type ObjectType = {
  [key: string]: string | number | boolean | undefined;
};

interface Column {
  label?: string | ReactNode;
  field?: string;
  width?: string;
  colSpan?: number;
  render?: (_data: ObjectType, _index: number) => ReactNode;
}

type RowData = {
  [key: string]: string | number | boolean | undefined;
};

interface TableProps {
  data?: RowData[];
  columns: Column[];
  isLoading?: boolean;
  pagination?: ReactNode;
  messageEmptyStatus?: string;
}

export const Table: FC<TableProps> = ({
  data = [],
  columns,
  pagination = false,
  isLoading = false,
  messageEmptyStatus,
}) => {
  return (
    <div className="shadow-md w-full rounded-b-lg">
      <table
        className={`w-full rounded-t-lg overflow-hidden ${
          data?.length > 0 && 'rounded-lg'
        }`}
      >
        <thead className="bg-white-20 border-b border-solid border-neutral_variant-50">
          <tr>
            {columns.map((column) => (
              <th
                key={column.field}
                colSpan={column?.colSpan || 1}
                className={`p-2 text-base font-medium text-neutral_variant-30  text-left ${
                  column.width ? column.width : 'w-auto'
                }`}
              >
                {column.label}
              </th>
            ))}
          </tr>
        </thead>
        {!isLoading && data?.length > 0 && (
          <tbody>
            {data?.map((rowData, rowIndex) => (
              <tr key={rowIndex}>
                {columns.map((column) => (
                  <td
                    key={column.field}
                    className={`w-auto p-2 capitalize ${column.width}`}
                  >
                    {column.render
                      ? column.render(rowData, rowIndex)
                      : column?.field && rowData[column.field]}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        )}
      </table>
      {isLoading && <Loading />}
      {!isLoading && data?.length === 0 && (
        <div className="w-full flex flex-col p-20 justify-center items-center">
          <span className="text-neutral_variant-50 text-3xl">
            Nenhum resultado encontrado
          </span>
          {!!messageEmptyStatus && (
            <span className="text-neutral_variant-80">
              {messageEmptyStatus}
            </span>
          )}
        </div>
      )}
      {!!pagination && (
        <div className="px-6 py-4 border-t border-solid border-neutral_variant-90">
          {pagination}
        </div>
      )}
    </div>
  );
};
