import { FC, ReactNode } from 'react';

interface ContentProps {
  noShadow?: boolean;
  background?: string;
  children?: ReactNode;
}

export const Content: FC<ContentProps> = ({
  children,
  noShadow,
  background,
}) => {
  const contentStyle = `flex justify-center w-full h-screen overflow-hidden ${background} ${
    !noShadow && 'shadow-[0_0px_32px_-32px_rgba(0,0,0,0.72)]'
  } max-w-[1366px]`;

  return <div className={contentStyle}>{children}</div>;
};
