import { Content } from './content';
import { Container } from './container';

export const Layout = {
  Content: Content,
  Container: Container,
};
