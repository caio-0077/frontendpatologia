import { FC } from 'react';

interface ContainerProps {
  background?: string;
  children?: React.ReactNode;
}

export const Container: FC<ContainerProps> = ({ children, background }) => {
  const containerStyle = `flex justify-center w-screen h-screen overflow-hidden ${background}`;

  return <div className={containerStyle}>{children}</div>;
};
