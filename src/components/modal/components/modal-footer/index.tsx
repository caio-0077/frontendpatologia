import { FC } from 'react';
import { Button } from 'components';

interface ModalFooterProps {
  textButton: string;
  onClose: () => void;
  onClick?: () => void;
  isLoading: boolean;
  hasButtonCancel: boolean;
  typeButton?: 'submit' | 'button';
}

export const ModalFooter: FC<ModalFooterProps> = ({
  onClick,
  onClose,
  isLoading,
  hasButtonCancel,
  textButton,
  typeButton,
}) => {
  return (
    <div
      className={`flex flex-row items-center gap-6 w-ful px-6 ${
        hasButtonCancel ? 'justify-end' : 'justify-center'
      }`}
    >
      {hasButtonCancel && (
        <Button variant="text" onClick={onClose}>
          Cancelar
        </Button>
      )}
      <Button
        onClick={onClick}
        width="w-[200px]"
        isLoading={isLoading}
        type={typeButton}
      >
        {textButton}
      </Button>
    </div>
  );
};
