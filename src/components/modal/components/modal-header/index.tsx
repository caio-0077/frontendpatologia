import { FC } from 'react';
import { Icon } from 'components';
import IconButton from '@mui/material/IconButton';

interface ModalHeaderProps {
  title: string;
  onClose: () => void;
}

export const ModalHeader: FC<ModalHeaderProps> = ({ title, onClose }) => {
  return (
    <div className="h-16 w-full bg-white-20 flex px-6 items-center justify-center relative flex-row">
      <span className="text-2xl text-primary-20">{title}</span>
      <div className="absolute right-0 pr-4">
        <IconButton onClick={onClose}>
          <Icon name="close" className="text-neutral_variant-30" />
        </IconButton>
      </div>
    </div>
  );
};
