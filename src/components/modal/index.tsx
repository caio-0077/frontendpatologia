import { FC, ReactNode, MouseEvent } from 'react';
import { Dialog, Grid } from '@mui/material';
import { ModalHeader, ModalFooter } from './components';

interface ModalProps {
  open: boolean;
  title: string;
  typeButton?: 'submit' | 'button';
  subTitle?: string;
  onSubmit?: () => void;
  children?: ReactNode;
  textButton?: string;
  isLoading?: boolean;
  clickOutside?: boolean;
  hasButtonCancel?: boolean;
  onClose: () => void;
  onClick?: () => void;
}

const paperProps = {
  style: {
    backgroundColor: '#FFF',
    borderRadius: '8px',
    width: '480px',
  },
};

export const Modal: FC<ModalProps> = ({
  open,
  title,
  onClose,
  onSubmit,
  children,
  subTitle,
  onClick,
  typeButton = 'submit',
  textButton = 'Salvar',
  isLoading = false,
  clickOutside = true,
  hasButtonCancel = true,
}) => {
  const handleOnSubmit = (event: MouseEvent<HTMLFormElement>) => {
    event.preventDefault();
    event.stopPropagation();
    if (onSubmit) onSubmit();
  };

  const handleClose = () => {
    if (clickOutside) onClose();
  };

  return (
    <Grid className="overflow-auto">
      <Dialog open={open} PaperProps={paperProps} onClose={handleClose}>
        <ModalHeader title={title} onClose={onClose} />
        <form
          className="py-6 gap-8 flex flex-col overflow-hidden"
          onSubmit={handleOnSubmit}
        >
          {!!subTitle && (
            <div className="px-6 w-full flex flex-row justify-center items-center text-center">
              <span className="text-lg text-neutral_variant-30">
                {subTitle}
              </span>
            </div>
          )}
          <div className="overflow-y-auto w-full overflow-hidden px-6">
            {children}
          </div>
          <ModalFooter
            onClick={onClick}
            onClose={onClose}
            isLoading={isLoading}
            textButton={textButton}
            typeButton={typeButton}
            hasButtonCancel={hasButtonCancel}
          />
        </form>
      </Dialog>
    </Grid>
  );
};
