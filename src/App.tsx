import { StrictMode } from 'react';

import { router } from 'routes';
import { useTheme } from 'hooks/contexts/use-theme';
import { RouterProvider } from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, createTheme } from '@mui/material/styles';

export const App = () => {
  const { theme } = useTheme();

  const systemTheme = createTheme({
    palette: {
      mode: theme,
    },
  });

  return (
    <StrictMode>
      <ThemeProvider theme={systemTheme}>
        <RouterProvider router={router} />
        <CssBaseline />
      </ThemeProvider>
    </StrictMode>
  );
};
