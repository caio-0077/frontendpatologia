import { FC, ReactNode } from 'react';

import { SignIn } from 'screens/auth';
import logoSBP from 'assets/sbp-logo.png';
import { redirect } from 'react-router-dom';
import { storage } from 'utils/local-storage';
import { useAuth } from 'hooks/contexts/use-auth';

interface PrivateRouteProps {
  element: ReactNode;
}

export const PrivateRoute: FC<PrivateRouteProps> = ({ element }) => {
  const { token } = useAuth();
  const tokenLocalStorage = localStorage.getItem(storage.token);

  if (!token || token === 'initial') {
    if (tokenLocalStorage) {
      return (
        <div className="absolute flex items-center justify-center w-full h-full bg-primary-20 fade-in">
          <img src={logoSBP} alt="spb-logo" className="w-[133px] h-[56px]" />
        </div>
      );
    }
    redirect('/login');
    return <SignIn />;
  }
  return element;
};
