export const enumRoutesAuth = {
  LOGIN: '/login',
  FORGOT: '/login/:tab',
  RESET: '/login/:tab/:token',
};
