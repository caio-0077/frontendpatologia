import { SignIn } from 'screens/auth';
import { Laboratories } from 'screens/laboratories';

import { LayoutRoutes } from './layout-routes';
import { PrivateRoute } from './private-routes';
import { createBrowserRouter } from 'react-router-dom';
import {
  enumRoutesAuth,
  enumRoutesLaboratories,
} from 'routes/enum-routes';

export const router = createBrowserRouter([
  {
    children: [
      {
        path: enumRoutesAuth.LOGIN,
        element: <SignIn />,
      },
      {
        path: enumRoutesAuth.RESET,
        element: <SignIn />,
      },
      {
        path: enumRoutesAuth.FORGOT,
        element: <SignIn />,
      },
      {
        path: '/',
        element: <PrivateRoute element={<LayoutRoutes />} />,
        children: [
          {
            path: enumRoutesLaboratories.LABORATORIES,
            element: <Laboratories />,
          },
        ],
      },
    ],
  },
]);
