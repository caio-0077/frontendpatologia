import { Suspense } from 'react';

import { DrawerMenu, Layout } from 'components';
import { Outlet, Await, useLoaderData } from 'react-router-dom';

export const LayoutRoutes = () => {
  const data = useLoaderData();

  return (
    <Suspense>
      <Await resolve={data}>
        <Layout.Container background="bg-background-0">
          <Layout.Content background="bg-background-0">
            <DrawerMenu />
            <Outlet />
          </Layout.Content>
        </Layout.Container>
      </Await>
    </Suspense>
  );
};
