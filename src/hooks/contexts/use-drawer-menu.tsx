import { FC, useState, useContext, createContext, ReactNode } from 'react';

interface DrawerMenuContextType {
  hasOpenOrClosedDrawer?: boolean;
  openDrawerMenu?: () => void;
  closeDrawerMenu?: () => void;
}

const DrawerMenuContext = createContext<DrawerMenuContextType | undefined>(
  undefined,
);

interface DrawerMenuProviderProps {
  children: ReactNode;
}

export const DrawerMenuProvider: FC<DrawerMenuProviderProps> = ({
  children,
}) => {
  const [hasOpenOrClosedDrawer, setHasOpenOrClosedDrawer] = useState(true);

  const openDrawerMenu = () => {
    setHasOpenOrClosedDrawer(true);
  };

  const closeDrawerMenu = () => {
    setHasOpenOrClosedDrawer(false);
  };

  return (
    <DrawerMenuContext.Provider
      value={{
        hasOpenOrClosedDrawer,
        openDrawerMenu,
        closeDrawerMenu,
      }}
    >
      {children}
    </DrawerMenuContext.Provider>
  );
};

export function useDrawerMenu() {
  const context = useContext(DrawerMenuContext);

  if (!context) {
    throw new Error(
      'useDrawerMenu deve ser usado dentro do DrawerMenuProvider',
    );
  }

  return context;
}
