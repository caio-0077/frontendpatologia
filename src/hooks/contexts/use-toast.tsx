import { FC, ReactNode, useContext, createContext } from 'react';

import { styled } from '@mui/system';
import {
  SnackbarProvider,
  MaterialDesignContent,
  useSnackbar,
} from 'notistack';

const Toast = styled(MaterialDesignContent)(() => ({
  '&.notistack-MuiContent-success': {
    fontWeight: '500',
    backgroundColor: '#008580',
  },
  '&.notistack-MuiContent-error': {
    fontWeight: '500',
    backgroundColor: '#93000A',
  },
  '&.notistack-MuiContent-warning': {
    fontWeight: '500',
    backgroundColor: '#EC9C00',
  },
}));

interface ToastData {
  message: string;
  type?: 'success' | 'error' | 'warning';
  autoHideDuration?: number;
}

interface ToastContextType {
  showToast: (_toastData: ToastData) => void;
}

interface ShowToastProviderProps {
  children: ReactNode;
}

const ToastContext = createContext<ToastContextType | undefined>(undefined);

interface ToastProviderProps {
  children: ReactNode;
}

const ToastProvider: FC<ToastProviderProps> = ({ children }) => {
  const { enqueueSnackbar } = useSnackbar();

  const showToast = ({
    message,
    type = 'success',
    autoHideDuration = 3000,
  }: ToastData) => {
    enqueueSnackbar(message, { variant: type, autoHideDuration });
  };

  return (
    <ToastContext.Provider value={{ showToast }}>
      {children}
    </ToastContext.Provider>
  );
};

export function useToast(): ToastContextType {
  const context = useContext(ToastContext);

  if (!context) {
    throw new Error('useToast deve ser usado dentro do ToastProvider');
  }

  return context;
}

export const ShowToastProvider: FC<ShowToastProviderProps> = ({ children }) => {
  return (
    <SnackbarProvider
      maxSnack={3}
      hideIconVariant
      Components={{
        success: Toast,
        error: Toast,
        warning: Toast,
      }}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
    >
      <ToastProvider>{children}</ToastProvider>
    </SnackbarProvider>
  );
};
