import {
  FC,
  useContext,
  createContext,
  ReactNode,
  useState,
  useEffect,
  useCallback,
} from 'react';
import { routesApi } from 'services/routes';
import { useToast } from 'hooks/contexts/use-toast';
import { storage, resetStorages } from 'utils/local-storage';
import { getErrorMessageServer } from 'utils/converters';

type User = {
  [key: string]: undefined;
};

type ForgotPasswordData = {
  email: string;
};

type ResetPasswordData = {
  token?: string;
  password: string;
};

type DataType = {
  token?: string | undefined;
  user: {
    [key: string]: undefined;
  };
};

interface AuthProviderProps {
  children: ReactNode;
}

interface LoginData {
  email: string;
  password: string;
}

interface AuthContextType {
  signIn: (_data: LoginData) => Promise<void>;
  signOut: () => void;
  user: User;
  token: string | undefined;
  isAuth: boolean;
  isAuthLoading: boolean;
  forgotPassword: (_data: ForgotPasswordData) => Promise<void>;
  isForgotPasswordLoading: boolean;
  resetPassword: (_data: ResetPasswordData) => Promise<void>;
  isResetPasswordLoading: boolean;
}

const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const AuthProvider: FC<AuthProviderProps> = ({ children }) => {
  const { showToast } = useToast();

  const [isAuth, setIsAuth] = useState(false);
  const [isAuthLoading, setIsAuthLoading] = useState(false);
  const [isForgotPasswordLoading, setIsForgotPasswordLoading] = useState(false);
  const [isResetPasswordLoading, setIsResetPasswordLoading] = useState(false);
  const [data, setData] = useState<DataType>({
    token: 'initial',
    user: {},
  });

  const signIn = useCallback(
    async (data: LoginData) => {
      try {
        setIsAuthLoading(true);
        const response = await routesApi.auth.signIn(data);
        const { token, user } = response.data;
        if (!token) {
          showToast({
            type: 'error',
            message: 'Não é possível conectar ao servidor',
          });
        }
        localStorage.setItem(storage.token, token);
        setIsAuth(true);
        setData({ token, user });
      } catch (error) {
        setIsAuth(false);
        if (error) showToast(getErrorMessageServer(error));
        throw new Error();
      } finally {
        setIsAuthLoading(false);
      }
    },
    [showToast],
  );

  const signOut = useCallback(() => {
    resetStorages();
    setData({ token: 'initial', user: {} });
    setIsAuth(false);
  }, []);

  const forgotPassword = useCallback(async (data: ForgotPasswordData) => {
    try {
      setIsForgotPasswordLoading(true);
      await routesApi.auth.forgotPassword(data);
    } catch (error) {
      throw new Error('Error no esqueceu sua senha');
    } finally {
      setIsForgotPasswordLoading(false);
    }
  }, []);

  const resetPassword = useCallback(
    async (data: ResetPasswordData) => {
      try {
        setIsResetPasswordLoading(true);
        if (!data.token) {
          return showToast({
            type: 'error',
            message: 'Token inválido',
          });
        }
        await routesApi.auth.resetPassword(data);
      } catch (error) {
        if (error) showToast(getErrorMessageServer(error));
        throw new Error();
      } finally {
        setIsResetPasswordLoading(false);
      }
    },
    [showToast],
  );

  useEffect(() => {
    const token = localStorage.getItem(storage.token);
    if (token) {
      routesApi.auth
        .validate()
        .then((response) => {
          setData({ token, user: response?.data?.user });
          setIsAuth(true);
        })
        .catch(() => {
          setData({ token: 'initial', user: {} });
          resetStorages();
          setIsAuth(false);
        });
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{
        signIn,
        token: data.token,
        user: data.user,
        isAuth,
        isAuthLoading,
        signOut,
        forgotPassword,
        isForgotPasswordLoading,
        resetPassword,
        isResetPasswordLoading,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth deve ser usado dentro do AuthProvider');
  }
  return context;
}
