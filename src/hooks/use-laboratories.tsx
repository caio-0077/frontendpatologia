import { useToast } from './contexts/use-toast';
import { routesApi } from 'services/routes';
import { useState, useCallback } from 'react';
import { getErrorMessageServer } from 'utils/converters';

type CSVDataType = {
  name?: string;
  file?: File | Blob | undefined;
  size?: string;
  error?: boolean;
};

type FiltersType = {
  page?: number;
  search?: string;
  orderDesc?: number;
  pageSize?: number;
  sortField?: string;
};

type CreateLaboratorieDataType = {
  companyName?: string;
  tradeName?: string;
  cnpj?: string;
  size?: string;
  street?: string;
  phone?: string;
};

type UpdateLaboratorieDataType = {
  id: string | undefined;
  data: {
    companyName?: string;
    tradeName?: string;
    cnpj?: string;
    size?: string;
    street?: string;
    phone?: string;
  };
};

interface LaboratorieDataType {
  id?: string;
  company_name?: string;
  trade_name?: string;
  cnpj?: string;
  size?: string;
  street?: string;
  number?: string;
  district?: string;
  city?: string;
  zip_code?: string;
  reference_point?: string;
  status?: string;
  phone?: string;
  created_at?: string;
  updated_at?: string;
}

type ContentType = {
  id?: string;
  company_name?: string;
  trade_name?: string;
  cnpj?: string;
  size?: string;
  street?: string;
  number?: string;
  city?: string;
  zip_code?: string;
  reference_point?: string;
  phone?: string;
  created_at?: string;
  updated_at?: string;
};

interface LaboratoriesDataType {
  content: ContentType[];
  pagination?: {
    page?: number;
    pageSize?: number;
    total?: number;
  };
}

export const useLaboratories = () => {
  const { showToast } = useToast();
  const [laboratories, setLaboratories] = useState<LaboratoriesDataType>();
  const [laboratorie, setLaboratorie] = useState<LaboratorieDataType>({});
  const [isLaboratoriesLoading, setIsLaboratoriesLoading] = useState(false);
  const [isLaboratorieLoading, setIsLaboratorieLoading] = useState(false);
  const [isCreateLaboratorieLoading, setIsCreateLaboratorieLoading] =
    useState(false);
  const [isUpdateLaboratorieLoading, setIsUpdateLaboratorieLoading] =
    useState(false);
  const [isSendCSVLoading, setIsSendCSVLoading] = useState(false);

  const getLaboratories = useCallback(
    async (filters: FiltersType) => {
      try {
        setIsLaboratoriesLoading(true);
        const response = await routesApi.laboratories.getLabs(filters);
        setLaboratories(response.data);
      } catch (error) {
        if (error) showToast(getErrorMessageServer(error));
      } finally {
        setIsLaboratoriesLoading(false);
      }
    },
    [showToast],
  );

  const getLaboratorie = useCallback(
    async (id: string) => {
      try {
        setIsLaboratorieLoading(true);
        const response = await routesApi.laboratories.getLab({ id });
        setLaboratorie(response.data);
      } catch (error) {
        if (error) showToast(getErrorMessageServer(error));
      } finally {
        setIsLaboratorieLoading(false);
      }
    },
    [showToast],
  );

  const createLaboratorie = useCallback(
    async (data: CreateLaboratorieDataType) => {
      try {
        setIsCreateLaboratorieLoading(true);
        await routesApi.laboratories.createLab(data);
        showToast({
          type: 'success',
          message: 'Laboratório adicionado com sucesso',
        });
      } catch (error) {
        if (error) showToast(getErrorMessageServer(error));
        throw new Error();
      } finally {
        setIsCreateLaboratorieLoading(false);
      }
    },
    [showToast],
  );

  const updateLaboratorie = useCallback(
    async ({ data, id }: UpdateLaboratorieDataType) => {
      try {
        setIsUpdateLaboratorieLoading(true);
        await routesApi.laboratories.updateLab({ data, id });
        showToast({
          type: 'success',
          message: 'Laboratório editado com sucesso',
        });
      } catch (error) {
        if (error) showToast(getErrorMessageServer(error));
        throw new Error();
      } finally {
        setIsUpdateLaboratorieLoading(false);
      }
    },
    [showToast],
  );

  const sendCSV = useCallback(
    async (data: CSVDataType) => {
      try {
        setIsSendCSVLoading(true);
        await routesApi.laboratories.sendCSV(data);
        showToast({
          type: 'success',
          message: 'Importação concluída com sucesso!',
        });
      } catch (error) {
        if (error) showToast(getErrorMessageServer(error));
        throw new Error();
      } finally {
        setIsSendCSVLoading(false);
      }
    },
    [showToast],
  );

  return {
    getLaboratories,
    laboratories,
    isLaboratoriesLoading,
    createLaboratorie,
    isCreateLaboratorieLoading,
    updateLaboratorie,
    isUpdateLaboratorieLoading,
    getLaboratorie,
    laboratorie,
    isLaboratorieLoading,
    sendCSV,
    isSendCSVLoading,
  };
};
