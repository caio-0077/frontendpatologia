import { FC, ReactNode } from 'react';

import { AuthProvider } from 'hooks/contexts/use-auth';
import { ThemeProvider } from 'hooks/contexts/use-theme';
import { ShowToastProvider } from 'hooks/contexts/use-toast';
import { DrawerMenuProvider } from 'hooks/contexts/use-drawer-menu';

interface AppProviderProps {
  children: ReactNode;
}

export const AppProvider: FC<AppProviderProps> = ({ children }) => (
  <ThemeProvider>
    <ShowToastProvider>
      <AuthProvider>
        <DrawerMenuProvider>{children}</DrawerMenuProvider>
      </AuthProvider>
    </ShowToastProvider>
  </ThemeProvider>
);
